### Description
Script didactique de démonstration de l'usage des options de la commande bash **find**.
Permet de compacter les fichiers logs, journaux, du répertoire */var/log/*, soit selon leur taille, soit selon leur ancienneté, en jours.
Propose de supprimer les fichiers de sauvegarde en *.tar.gz* de plus de deux mois d'ancienneté du dossier */var/log/*.

#### Version 1.0
* Version console de commande.
* Choix de la taille ou de l'ancienneté.

### Aller plus loin
* La variable **REP** peut être changée pour viser d'autres dossiers.
* La variable **DIR** peut être décommentée et renseignée pour que les sauvegardes se fassent dans un autre dossier que le */home/* de l'utilisateur. En remplaçant *"~"* par **$DIR** :
`find $REP *.log -atime +$temps -exec tar -zcf ~/log$DATE.tar.gz {} \;` par `find $REP *.log -atime +$temps -exec tar -zcf $DIR/log$DATE.tar.gz {} \;`

